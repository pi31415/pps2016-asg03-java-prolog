% symbol(+OnBoard,-OnScreen): symbols for the board and their rendering
symbol(null,'_').
%if wants to start O invert this
symbol(p1,'X').
symbol(p2,'O').

% result(+OnBoard,-OnScreen): result of a game
result(even,"even!").
result(p1,"player 1 wins").
result(p2,"player 2 wins").

% other_player(?Player,?OtherPlayer)
other_player(p1,p2).
other_player(p2,p1).

% render(+List): prints a TTT table (9 elements list) on console
render(L) :- convert_symbols(L,[A,B,C,D,E,F,G,H,I]),print_row(A,B,C),print_row(D,E,F),print_row(G,H,I).
convert_symbols(L,L2) :- findall(R,(member(X,L),symbol(X,R)),L2).
print_row(A,B,C) :- put(A),put(' '),put(B),put(' '),put(C),nl.

% render4(+List): prints a TTT table (16 elements list) on console
render(L) :- convert_symbols(L,[A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P]),print_row(A,B,C,D),print_row(E,F,G,H),print_row(I,J,K,L),print_row(M,N,O,P).
print_row4(A,B,C,D) :- put(A),put(' '),put(B),put(' '),put(C),put(' '),put(D),nl.


% render(+List,+Result): prints a TTT table plus result
render_full(L,Result) :- result(Result,OnScreen),print(OnScreen),nl,render(L),nl,nl.

% render(+List,+Result): prints a TTT table plus result
render_full4(L,Result) :- result(Result,OnScreen),print(OnScreen),nl,render4(L),nl,nl.

% create_board(-Board): creates an initially empty board
create_board(B):-create_list(9,null,B).
create_list(0,_,[]) :- !.
create_list(N,X,[X|T]) :- N2 is N-1, create_list(N2,X,T).

% create_board4(-Board): creates an initially empty board (4)
create_board4(B):-create_list(16,null,B).

% next_board(+Board,+Player,?NewBoard): finds (zero, one or many) new boards as Player moves
next_board([null|B],PL,[PL|B]).
next_board([X|B],PL,[X|B2]):-next_board(B,PL,B2).

% final(+Board,-Result): checks where the board is final and why
final(B,p1) :- finalpatt(P), match(B,P,p1),!.
final(B,p2) :- finalpatt(P), match(B,P,p2),!.
final(B,even) :- not(member(null,B)).

% final4(+Board,-Result): checks where the board is final and why in 4 by 4
final4(B,p1) :- finalpatt4(P), match(B,P,p1),!.
final4(B,p2) :- finalpatt4(P), match(B,P,p2),!.
final4(B,even) :- not(member(null,B)).


% match(Board,Pattern,Player): checks if in the board, the player matches a winning pattern
match([],[],_).
match([M|B],[x|P],M):-match(B,P,M).
match([_|B],[o|P],M):-match(B,P,M).

% finalpatt(+Pattern): gives a winning pattern
finalpatt([x,x,x,o,o,o,o,o,o]).
finalpatt([o,o,o,x,x,x,o,o,o]).
finalpatt([o,o,o,o,o,o,x,x,x]).
finalpatt([x,o,o,x,o,o,x,o,o]).
finalpatt([o,x,o,o,x,o,o,x,o]).
finalpatt([o,o,x,o,o,x,o,o,x]).
finalpatt([x,o,o,o,x,o,o,o,x]).
finalpatt([o,o,x,o,x,o,x,o,o]).

% game(+Board,+Player,-FinalBoard,-Result): finds one (zero, one or many) final boards and results
game(B,_,B,Result) :- final(B,Result),!.
game(B,PL,BF,Result):- next_board(B,PL,B2), other_player(PL,PL2),game(B2,PL2,BF,Result).

game4(B,_,B,Result) :- final4(B,Result),!.
game4(B,PL,BF,Result):- next_board(B,PL,B2), other_player(PL,PL2),game4(B2,PL2,BF,Result).

% statistics(+Board,+Player,+Result,-Count): counts how many time Res will happen 
statistics(B,P,Res,Count) :- findall(a, game(B,P,_,Res),L), length(L,Count).

%suggestMove(B,P,_,Res) :-


finalpatt4([x,x,x,x,o,o,o,o,o,o,o,o,o,o,o,o]).
finalpatt4([o,o,o,o,x,x,x,x,o,o,o,o,o,o,o,o]).
finalpatt4([o,o,o,o,o,o,o,o,x,x,x,x,o,o,o,o]).
finalpatt4([o,o,o,o,o,o,o,o,o,o,o,o,x,x,x,x]).
finalpatt4([x,o,o,o,x,o,o,o,x,o,o,o,x,o,o,o]).
finalpatt4([o,x,o,o,o,x,o,o,o,x,o,o,o,x,o,o]).
finalpatt4([o,o,x,o,o,o,x,o,o,o,x,o,o,o,x,o]).
finalpatt4([x,o,o,o,o,x,o,o,o,o,x,o,o,o,o,x]).
finalpatt4([o,o,o,x,o,o,x,o,o,x,o,o,x,o,o,o]).

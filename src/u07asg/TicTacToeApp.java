package u07asg;


import javax.swing.*;
import java.awt.*;
import java.util.Optional;

/** A not so nicely engineered App to run TTT games, not very important.
 * It just works..
 */
public class TicTacToeApp {

    private final int boardSize = 4;
    private final int maxMoves = boardSize * boardSize;
    private final int jPanelSize = maxMoves + (boardSize - 1);//it includes big lines
    private final TicTacToe[] ttt = new TicTacToe[boardSize * boardSize];
    private final TicTacToe tttBig;
    private final JButton[][][][] board = new JButton[boardSize][boardSize][boardSize][boardSize];
    private final JButton exit = new JButton("Exit");
    private final JFrame frame = new JFrame("TTT");
    private boolean[] finished = new boolean[maxMoves];
    private Player turn = Player.PlayerX;
    private int[] moves = new int[maxMoves];
    private String[] winner = new String[maxMoves];
    private int bigMoves = 0;

    private void changeTurn(){
        this.turn = this.turn == Player.PlayerX ? Player.PlayerO : Player.PlayerX;
    }

    public TicTacToeApp(TicTacToe[] ttt, TicTacToe tttBig) throws Exception {
        for(int i = 0; i< maxMoves ; i++ ) {
            this.ttt[i] = ttt[i];
            moves[i] = 0;
            finished[i] = false;
            winner[i] = "";
        }
        this.tttBig = tttBig;
        initPane();
    }
    
    private void humanMove(int bigRow, int bigColumn, int smallRow, int smallColumn){
        int boardNumber = bigRow*boardSize + bigColumn;
        if (ttt[boardNumber].move(turn,smallRow,smallColumn)){
            board[bigRow][bigColumn][smallRow][smallColumn].setText(turn == Player.PlayerX ? "X" : "O");
            moves[boardNumber]++;
            changeTurn();
            if (moves[boardNumber] > boardSize){
                System.out.println(boardNumber + " X winning count: "+ttt[boardNumber].winCount(turn, Player.PlayerX));
                System.out.println(boardNumber + " O winning count: "+ttt[boardNumber].winCount(turn, Player.PlayerO));
            }
        }
        Optional<Player> victory = ttt[boardNumber].checkVictory();
        if (victory.isPresent()){
            exit.setText(victory.get()+" won!");
            finished[boardNumber]=true;
            winner[boardNumber] = victory.get().toString();
            tttBig.move(victory.get(), bigRow, bigColumn);
            bigMoves ++;
            if (bigMoves > boardSize) {
                System.out.println("Big Board X winning count: "+tttBig.winCount(turn, Player.PlayerX));
                System.out.println("Big Board O winning count: "+tttBig.winCount(turn, Player.PlayerO));
                Optional<Player> vic = tttBig.checkVictory();
                if (bigMoves < (boardSize * boardSize)) {
                    if (vic.isPresent()) {
                        exit.setText("Big Board Winner " + vic.get().toString());
                    }
                }
                else {
                    exit.setText("Big Board Even");
                }
            }
            return;
        }
        if (ttt[boardNumber].checkCompleted()){
            exit.setText("Even!");
            finished[boardNumber]=true;
            winner[boardNumber] = "Even!";
            bigMoves ++;
            if (bigMoves == (maxMoves)) {
                exit.setText("Big Board Even");
            }
            return;
        }
    }

    private void initPane(){
        frame.setLayout(new BorderLayout());
        JPanel b=new JPanel(new GridLayout(jPanelSize,jPanelSize));
        for (int bigRow = 0; bigRow < boardSize; bigRow++) {
            for (int SmallRow = 0; SmallRow < boardSize; SmallRow++) {
                for (int bigColumn = 0; bigColumn < boardSize; bigColumn++) {
                    for (int smallColumn = 0; smallColumn < boardSize; smallColumn++) {
                        final int bC = bigColumn;
                        final int sC = smallColumn;
                        final int bR = bigRow;
                        final int sR = SmallRow;
                        board[bigRow][bigColumn][SmallRow][smallColumn] = new JButton("");
                        b.add(board[bigRow][bigColumn][SmallRow][smallColumn]);
                        board[bigRow][bigColumn][SmallRow][smallColumn].addActionListener(e -> {
                            if (!finished[bR*boardSize+bC]) humanMove(bR, bC, sR, sC);
                        });
                    }
                    if(bigColumn< (boardSize-1))
                        b.add(new JButton("---"));
                }
            }
            if (bigRow<(boardSize-1)) {
                for (int a = 0; a< jPanelSize ; a++) {
                    b.add(new JButton("---"));
                }
            }
        }

        JPanel s=new JPanel(new FlowLayout());
        s.add(exit);
        exit.addActionListener(e -> System.exit(0));
        frame.add(BorderLayout.CENTER,b);
        frame.add(BorderLayout.SOUTH,s);
        frame.setSize(500,550);
        frame.setVisible(true);
    }

    public static void main(String[] args){
        try {
            TicTacToeImpl[] ttt = new TicTacToeImpl[16];
            for (int i = 0; i < 16; i++) {
                ttt[i] = new TicTacToeImpl("src/u07asg/ttt.pl");
            }
            TicTacToe ttt2 = new TicTacToeImpl("src/u07asg/ttt.pl");
            new TicTacToeApp(ttt, ttt2);
        } catch (Exception e) {
            System.out.println("Problems loading the theory");
        }
    }
}
